<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookController extends Controller
{
    /**
     * index
     *
     * @return void
     */
    public function index()
    {
        //get data from table categories
        $books = Book::latest()->get();

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Books',
            'data'    => $books 
        ], 200);

    }
    
     /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    public function show($id)
    {
        //find category by ID
        $books = Book::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Book Data',
            'data'    => $books
        ], 200);

    }
    
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'category_id'   => 'required',
            'author_id'   => 'required',

        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //save to database
        $book = Books::create([
            'title'     => $request->category_name,
            'category_id'     => $request->category_id,
            'author_id'     => $request->author_id,
            
        ]);

        //success save to database
        if($book) {

            return response()->json([
                'success' => true,
                'message' => 'Book Created',
                'data'    => $book 
            ], 201);

        } 

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Book Failed to Save',
        ], 409);

    }
    
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $category
     * @return void
     */
    public function update(Request $request, Book $book)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'category_id'   => 'required',
            'author_id'   => 'required',
           
        ]);
        
        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find category by ID
        $book = Book::findOrFail($book->id);

        if($book) {

            //update book
            $book->update([
                'title'     => $request->title,
                'category_id'     => $request->category_id,
                'author_id'     => $request->author_id,
               
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Book Updated',
                'data'    => $book 
            ], 200);

        }

        //data category not found
        return response()->json([
            'success' => false,
            'message' => 'Book Not Found',
        ], 404);

    }
    
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find category by ID
        $book = Book::findOrfail($id);

        if($book) {

            //delete book
            $book->delete();

            return response()->json([
                'success' => true,
                'message' => 'Book Deleted',
            ], 200);

        }

        //data category not found
        return response()->json([
            'success' => false,
            'message' => 'Book Not Found',
        ], 404);
    }
}
